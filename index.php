<?php
session_start();
ini_set('session.gc_maxlifetime', 3600);

if(!isset($_SESSION['id'])){
    header('location: home.php');
}

header('location: home.php');
?>

