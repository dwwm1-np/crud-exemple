<?php
if (isset($_SESSION['id'])) {
    header("location: user.php");
}
?>


<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="./css/nav.css">
    <link rel="stylesheet" href="./css/login.css">
    <title>Login</title>
</head>

<body>

    <nav>
        <ul>
            <li><a href="home.php">Home</a></li>
            <li><a href="login.php">Connexion</a></li>
            <li><a href="register.php">Register</a></li>
        </ul>
    </nav>

    <div class="container">

        <?php
        if (isset($_GET['error'])) {
            echo "<h2>L'utilisateur existe déjà</h2>";
        }
        ?>

        <form action="users/Create.php" method="POST">
            <label for="username">USERNAME:</label>
            <input type="text" name="username" required>
            <label for="email">MAIL:</label>
            <input type="email" name="email" required>
            <label for="password">PASSWORD</label>
            <input type="password" name="password" id="password"  required>
            <div id="password-message">
                <h5 class="invalid" id="message-size">8 charactères requis</h5>
                <h5 class="invalid" id="message-miniscule">Une minuscule</h5>
                <h5 class="invalid" id="message-majuscule">Une majuscule</h5>
                <h5 class="invalid" id="message-number">Une chiffre</h5>
                <h5 class="invalid" id="message-specials-char">Un charactère spéciale</h5>

            </div>
            <input type="submit" id="submit" value="submit">
        </form>
    </div>

    <script src="./js/password.js"> </script>
</body>

</html>