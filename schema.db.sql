CREATE DATABASE login_exemple;

USE login_exemple;

CREATE TABLE users (id INT PRIMARY KEY AUTO_INCREMENT, username VARCHAR(50), email VARCHAR(50), password VARCHAR(50), session VARCHAR(255));

CREATE TABLE todo (id INT PRIMARY KEY AUTO_INCREMENT, user_id INT, name VARCHAR(50), description VARCHAR(255), date TIMESTAMP);