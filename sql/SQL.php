<?php

class SQL {

    private $host;
    private $password;
    private $mysqli;

    function __construct($host, $password) {
        $this->host = $host;
        $this->password = $password;
        $this->open_connection();
    }

    public function open_connection(){
        $this->mysqli = new mysqli($this->host, "root", "strong", "login_exemple");
    }

    public function close_connection(){
        $this->mysqli->close();
    }

    public function execute_query($query){
        $stmt = $this->mysqli->prepare($query);
        $stmt->execute();
        
        return $stmt->get_result();
    }

    public function create_user($username, $password, $mail, $session){
        $query = "INSERT INTO users (username, password, email, session) VALUES ('$username', '$password','$mail', '$session')";
        $this->execute_query($query);
    }

    public function new_session($username, $id){
        $query = "UPDATE users SET session = '$id' WHERE username = '$username'";
        $this->execute_query($query);
    }

    public function delete_session($id){
        $query = "UPDATE users SET session = 'NULL' WHERE session = '$id'";
        $this->execute_query($query);
    }

    public function get_user($user, $password){
        $query = "SELECT * FROM users WHERE username = '$user' OR email = '$user' AND password = '$password'";
        return $this->execute_query($query)->fetch_assoc();
    }

    public function user_exist($username){
        $query = "SELECT * FROM users WHERE username = '$username' OR email = '$username'";

        $user = $this->execute_query($query)->fetch_assoc();

        if(!empty($user)){
            return true;
        }

        return false;

    }

    public function get_user_id($session) {
        $query = "SELECT * FROM users WHERE session = '$session'";
        return $this->execute_query($query)->fetch_assoc()['id'];
    }

    public function user_todo($session){
        $query = "SELECT todo.* FROM users INNER JOIN todo ON users.id = todo.user_id WHERE users.session = '$session'";
        return $this->execute_query($query)->fetch_all();
    }

    public function user_todo_with_id($session, $id){
        $query = "SELECT todo.* FROM users INNER JOIN todo ON users.id = todo.user_id WHERE todo.id = $id";
        return $this->execute_query($query)->fetch_assoc();
    }

    public function insert_new_task($session, $name, $description, $date){
        $user_id = $this->get_user_id($session);
        $query = "INSERT INTO todo (user_id, name, description, date) VALUES ('$user_id', '$name', '$description', '$date')";
        return $this->execute_query($query);
    }

    public function delete_task($task_id){
        $query = "DELETE FROM todo WHERE id = $task_id";
        $this->execute_query($query);
    }

    public function update_task($task_id, $user_id, $name, $description, $date){
        $query = "UPDATE todo SET name = '$name', description = '$description', date= '$date' WHERE id = $task_id AND user_id = $user_id";
        $this->execute_query($query);
    }

    public function get_task($task){
        $query = "SELECT * FROM todo WHERE id = $task";
        return $this->execute_query($query)->fetch_assoc();
    }

}


