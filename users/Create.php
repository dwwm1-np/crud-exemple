<?php
session_start();
require "./../sql/SQL.php";

mysqli_report(MYSQLI_REPORT_ERROR | MYSQLI_REPORT_STRICT);


$mail = $_POST['email'];
$user = $_POST['username']; 
$password = $_POST['password'];


$sql = new SQL("127.0.0.1:3306", "strong");

$exist = $sql->user_exist($mail);

if($exist){
    header("location: ../register.php?error=true");
    return;
}

$password_crypt = password_hash($password, PASSWORD_BCRYPT);

$_SESSION['id'] = uniqid();

$user = $sql->create_user($user, $password_crypt, $mail, $_SESSION['id']);

$sql->new_session($user, $_SESSION['id']);

header("location: ../home.php");

?>