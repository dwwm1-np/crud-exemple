<?php

require "./../sql/SQL.php";

$user = $_POST['username']; 
$password = $_POST['password'];


$sql = new SQL("127.0.0.1:3306", "strong");

$user = $sql->get_user($user, $password);

$password_user = $user['password'];


if(password_verify($password, $password_user)){

    session_start();
    $_SESSION['id'] = uniqid();
    $sql->new_session($user['username'], $_SESSION['id']);
    header('location: ../home.php');

    return;
}

header('location: ../login.php?error=true');