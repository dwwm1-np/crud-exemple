const input_password = document.getElementById('password');

const error = document.getElementById('password-message');



const message_minuscule = document.getElementById('message-miniscule');
const message_majuscule = document.getElementById('message-majuscule');
const message_number = document.getElementById('message-number');
const message_special_chars = document.getElementById('message-specials-char');

const message_size = document.getElementById('message-size');

input_password.onblur = () => {
    error.style.display = "none";

}

input_password.onfocus = () => {
    error.style.display = "block";

}


input_password.addEventListener('keyup', (e)=> {
    const value = e.target.value;

    console.log('re');

    let minuscule = /[a-z]/g;
    let majuscule = /[A-Z]/g;
    let number = /[0-9]/g;
    let special_chars = /[$&+,:;=?@#|'<>.^*()%!-]/g;

    checkChars(message_minuscule, value, minuscule);
    checkChars(message_majuscule, value, majuscule);
    checkChars(message_number, value, number);
    checkChars(message_special_chars, value, special_chars);

    if(value.length >= 8 ){
        message_size.classList.add('valid')
        message_size.classList.remove('invalid')
    }else {
        message_size.classList.add('invalid')
        message_size.classList.remove('valid')
    }
    

})

function checkChars(text, value, regex){
    if(value.match(regex)){
        text.classList.add('valid');
        text.classList.remove('invalid');
    } else {
        text.classList.add('invalid');
        text.classList.remove('valid');
    }
}