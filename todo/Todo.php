<?php 


require "sql/SQL.php";


mysqli_report(MYSQLI_REPORT_ERROR | MYSQLI_REPORT_STRICT);

if(!isset($_SESSION['id'])){
    return;
}

$sql = new SQL("127.0.0.1:3306", "strong");

$todo = $sql->user_todo($_SESSION['id']);


?>

<div class="todo">
    
    <form action="todo/Task.php" method="POST">
        <label for="task_name">Nom de la tâche</label>
        <input type="text" name="task_name">
        <label for="task_desc">Description</label>
        <input type="text" name="task_desc">
        <input type="submit" value="Ajouter" id="submit_task">
    </form>
    <table>
        <thead>
            <tr>
                <td>Nom</td>
                <td>Description</td>
                <td>Date</td>
                <td></td>
                <td></td>
            </tr>
        </thead>

        <tbody>
        <?php


        if(empty($todo)){
            return;
        }

        foreach($todo as $values){


            echo "<tr>";
            echo "<td>";
            echo $values[2];
            echo "</td>";
            echo "<td>";
            echo $values[3];
            echo "</td>";
            echo "<td>";
            echo $values[4];
            echo "</td>";
            echo "<td>";
            echo "<a href='todo/edit.php?task=$values[0]'>Edit</a>";
            echo "</td>";
            echo "<td>";
            echo "<a href='todo/delete.php?task=$values[0]'>Delete</a>";
            echo "</td>";
            echo "</tr>";
        }
        ?>
        </tbody>
    </table>
</div>
