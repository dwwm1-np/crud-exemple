<?php

session_start();

require "../sql/SQL.php";

$sql = new SQL("127.0.0.1:3306", "strong");


$session = $_SESSION['id'];


if(!isset($session)){
    header('location: ../home.php');
}

if(isset($_GET['task'])){
    $user = $sql->get_user_id($session);
    $todo = $sql->get_task($_GET['task']);

    $task_id = $_GET['task'];
    
    if($todo['user_id'] != $user){
        header('location: ../home.php');
    }

    $sql->delete_task($task_id);

}

header('location: ../home.php');
