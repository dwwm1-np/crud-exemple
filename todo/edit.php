<?php

session_start();

require "../sql/SQL.php";

$sql = new SQL("127.0.0.1:3306", "strong");


$session = $_SESSION['id'];


if(!isset($session)){
    header('location: ../home.php');
}

if(!isset($_GET['task']) && empty($_POST)){
    echo $_GET['task'];
    header('location: ../home.php');
}

$task_id;

if(isset($_GET['task'])){
    $user = $sql->get_user_id($session);
    $todo = $sql->get_task($_GET['task']);

    $task_id = $_GET['task'];
    
    if($todo['user_id'] != $user){
        header('location: ../home.php');
    }
}

if(!empty($_POST)){
    $date = date("Y-m-d H:i:s");

    $task_id = $_POST['task_id'];

    $user = $sql->get_user_id($session);


    $sql->update_task($_POST['task_id'], $user, $_POST['task_name'],  $_POST['task_desc'], $date);

    header('../home.php');
}


?>


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="../css/nav.css">
    <link rel="stylesheet" href="../css/index.css">
    <title>Edit</title>
</head>
<body>
    <nav>
        <ul>
            <li><a href="../home.php">Home</a></li>

            <?php
            
                if(!isset($_SESSION['id'])){
                    echo "<li><a href='login.php'>Connexion</a></li>";
                    echo "<li><a href='register.php'>Register</a></li>";
                }else {
                    echo "<li><a href='users/Deconnexion.php'>Deconnexion</a></li>";

                }
            ?>
                
        </ul>
    </nav>

    <div class="container">
        <form action="edit.php" method="post">
            <?php 
                echo "<input type='hidden' name='task_id' value='$task_id'>";
            ?>
            <label for="task_name">Nom de tâche</label>
            <input type="text" name="task_name">
            <label for="task_desc">Description de la tâche</label>
            <input type="text" name="task_desc">
            <input type="submit" value="Editer la tâche">
        </form>
    </div>
</body>
</html>


