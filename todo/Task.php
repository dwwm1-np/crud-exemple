<?php

session_start();

require "../sql/SQL.php";

$sql = new SQL("127.0.0.1:3306", "strong");

if(!isset($_SESSION['id'])){
    header('location: ../home.php');
}

$name = $_POST['task_name'];
$desc = $_POST['task_desc'];
$date = date("Y-m-d H:i:s");

$user_session = $_SESSION['id'];

$sql->insert_new_task($user_session, $name, $desc, $date);

header('location: ../home.php');