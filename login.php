<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="./css/nav.css">
    <link rel="stylesheet" href="./css/login.css">
    <title>Login</title>
</head>

<body>


    <nav>
        <ul>
            <li><a href="home.php">Home</a></li>
            <li><a href="login.php">Connexion</a></li>
            <li><a href="register.php">Register</a></li>
        </ul>
    </nav>
   
    <div class="container">

        <?php    
            if(isset($_GET['error'])){
                echo "<h2>Indentifiant ou mot de passe incorrecte</h2>";
            }
        ?>

        <form action="users/Connect.php" method="POST">
            <label for="username">USERNAME ou EMAIL :</label>
            <input type="text" name="username" required>
            <label for="password">PASSWORD : </label>
            <input type="password" name="password" id="password" required>
            <input type="submit" id="submit" value="submit">
        </form>
    </div>
    
</body>

</html>